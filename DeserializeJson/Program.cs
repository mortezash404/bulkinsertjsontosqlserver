﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Dapper;
using Z.Dapper.Plus;

namespace BulkInsert
{
    public class Program
    {
        static void Main(string[] args)
        {
            var text = File.ReadAllText("E:\\generated01.json");

            var persons = text.Deserialize<List<Person>>();

            var stopwatch = Stopwatch.StartNew();

            // 0) bad way : using EFCore

            //using (var ctx = new AppDbContext())
            //{
            //    ctx.Persons.AddRangeAsync(persons);
            //    ctx.SaveChanges();
            //}

            // 1) better way : using EFCore.BulkExtensions

            //using (var ctx = new AppDbContext())
            //{
            //    ctx.BulkInsert(persons);
            //    ctx.SaveChanges();
            //}


            // 2) more better way : using DapperPlus

            //using (var conn = new SqlConnection("Server=.;Database=EFCoreDb;Trusted_Connection=True;"))
            //{
            //    conn.Open();

            //    conn.InsertBulk(persons);

            //    conn.Close();
            //}

            // 3) best way? : using Z.EntityFramework.Extensions

            //using (var ctx = new AppDbContext())
            //{
            //    ctx.BulkInsert(persons);
            //    ctx.BulkSaveChanges();
            //}

            // 4) best way? : using Z.dapper.plus

            using (var conn = new SqlConnection("Server=.;Database=EFCoreDb;Trusted_Connection=True;"))
            {
                conn.Open();

                var dapperPlusActionSet = conn.BulkMerge(persons);

                //dapperPlusActionSet.Actions.FirstOrDefault().

                conn.Close();
            }


            stopwatch.Stop();

            Console.WriteLine(stopwatch.ElapsedMilliseconds + " MSec");

            Console.ReadKey();

        }
    }
}





            
