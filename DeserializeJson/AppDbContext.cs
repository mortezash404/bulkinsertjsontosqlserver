﻿using DeserializeJson;
using Microsoft.EntityFrameworkCore;

namespace BulkInsert
{
    public class AppDbContext : DbContext
    {
        private const string _connectionString = "Server=.;Database=EFCoreDb;Trusted_Connection=True;";
        public DbSet<Person> Persons { get; set; }
        //public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        //{
            
        //}

        public AppDbContext()
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");
            modelBuilder.Entity<Person>().Property(p => p.Name).HasMaxLength(50);

            base.OnModelCreating(modelBuilder);
        }
    }
}
