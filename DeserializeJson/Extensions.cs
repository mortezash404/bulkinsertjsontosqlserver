﻿using System;
using Newtonsoft.Json;

namespace BulkInsert
{
    public static class Extensions
    {
        private static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            DefaultValueHandling = DefaultValueHandling.Include,
            Formatting = Formatting.None,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        public static T Deserialize<T>(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return default(T);
            try
            {
                return JsonConvert.DeserializeObject<T>(value, JsonSerializerSettings);
            }
            catch (Exception e)
            {
                return default(T);
            }
        }
    }
}
