﻿using Newtonsoft.Json;

namespace BulkInsert
{
    public class Person
    {
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("age")]
        public int Age { get; set; }
    }
}
